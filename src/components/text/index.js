import styled from 'styled-components';

const Text = styled.div`
  text-align: ${props => props.align ?? 'left'};
  display: ${props => props.block ?? 'block'};
  font-size: ${props => props.fontSize ?? 16}px;
  font-weight: ${props => props.fontWeight ?? 500};
  color: ${props => props.color ?? "#ffffff"};
  font-style: ${props => props.fontStyle ?? 'normal'};
  line-height: ${props => props.lineHeight ?? 'normal'};

  @media only screen and (max-width: 400px) {
    font-size: ${({fontSize = 14}) => (fontSize ? fontSize - 1 : fontSize)}px;
  }

  @media only screen and (max-width: 320px) {
    font-size: ${({fontSize = 14}) => (fontSize ? fontSize - 2 : fontSize)}px;
  }
`;

export {Text};
