import {checkContainsLocation, getPlaces} from '@/helpers';
import {AutoComplete} from 'antd'
import {useEffect, useState} from 'react'
import Link from 'next/link'
import {setupMap} from '@/helpers'
import {LOCATIONS, PolygonPaths} from '@/constants'
import {Text} from '@/components';

let timeout = null
const SearchInput = (props) => {
  const {placeholder} = props;

  const [value, setValue] = useState('');
  const [options, setOptions] = useState([]);

  const onChange = value => setValue(value)

  const handleSearch = async () => {
    const {data} = await getPlaces(value);
    const origin = data?.[0];
    const place = origin?.geometry?.location;
    if (!place) return;

    const result = Object.keys(PolygonPaths).find(
      key => checkContainsLocation(place, PolygonPaths[key])
    );

    const location = LOCATIONS.find(item => {
      return item.polygons.includes(result)
    })

    if(!location) {
      setOptions([{label: 'There are no stores near you'}])
      return;
    }

    const renderLabel = (destination) => {
      return (
        <Link href={destination.website}>
          <div key={destination.name}>
            <Text color="#484848">{destination.name}</Text>
          </div>
        </Link>
      )
    }

    const label = renderLabel(location)

    setOptions([{label}])
  }

  // setup maps
  useEffect(() => {
    setupMap()
  }, [])

  useEffect(() => {
    clearTimeout(timeout);
    if (!value) {
      setOptions([]);
      return;
    }
    timeout = setTimeout(handleSearch, 300)
  }, [value])

  return (
    <AutoComplete
      allowClear
      style={{width: "100%"}}
      size="large"
      value={value}
      onChange={onChange}
      options={options}
      placeholder={placeholder}
    />
  )
}

export {SearchInput}
