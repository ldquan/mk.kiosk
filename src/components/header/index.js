import styled from 'styled-components'
import {Col, Row} from 'antd'
import Image from 'next/image'
import {InstagramOutlined, FacebookOutlined} from '@ant-design/icons'

import {Clearfix, Text} from '@/components'

import {SearchInput} from './search-location'

const HeaderLayout = styled.div`
  position: relative;
  background: #a81e24;

  @media only screen and (max-width: 512px) {
    height: 125vh; 
  }

  @media only screen and (max-width: 400px) {
    height: 140vh; 
  }

  @media only screen and (max-width: 320px) {
    height: 150vh; 
  }

  & > img{
    width: 100%;
    @media only screen and (max-width: 512px) {
      display: none
    }
  }
`
const Overlay = styled.div`
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
  width: 100%;
  padding: 0 21vw;

  @media only screen and (max-width: 1024px) {
    padding: 0 10vw;
  }
`

const ImageWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  img {
    max-width: 95vw;
  }
`
const ImageLogoWrapper = styled.div`
  img{
    @media only screen and (max-width: 521px) {
      max-width: 80vw;
    }
  }
`
const ServiceWrapper = styled.div`
  margin-top: 10vh;
  width: 100%;
  @media only screen and (max-width: 1920px) {
    margin-top: 28vh;
  }

  @media only screen and (max-width: 1440px) {
    margin-top: 12vh;
  }

  @media only screen and (max-width: 1024px) {
    bottom: 6vh;
  }

  @media only screen and (max-width: 516px) {
    position: relative;
  }

  &>div:first-child{
    margin: 0 auto;
    width: 60vw;
    height: 30vh;
    border-radius: 4px;
    background: #ffffff;
    opacity: 0.8;

    @media only screen and (max-width: 1440px) {
      height: 28vh;
    }

    @media only screen and (max-width: 1024px) {
      height: 25vh;
    }

    @media only screen and (max-width: 512px) {
      padding: 20px 0;
      height: unset;
      width: 80vw;
      opacity: 1
    }
  }
`

const Service = styled(Col)`
  &>div{
    flex-direction: column;
  }

  img{
    @media only screen and (max-width: 1440px) {
      width: 80%
    }

    @media only screen and (max-width: 1024px) {
      width: 60%
    }
  }
`

const i18n = {
  login: 'LOGIN',
  service: 'Cyprus\' #1 Kiosk delivery service',
  question: 'Hi! What can we bring you today?',
  inputAddress: 'Please Enter Your Address',
  getall: 'Get all your kiosk and and small shopping needs dilivered straight away!',
  howto: 'How do you order? Easy!',
  steps: {
    input: 'Enter Your Address',
    choose: 'Choose Items & Checkout',
    dilivered: 'Dilivered Straight Away!',
  }
}

const iconStyle = {
  color: '#ffffff',
  fontSize: 30,
  marginRight: 10,
  cursor: 'pointer'
}

const Header = () => (
  <HeaderLayout>
    <img src="/images/header-bg-main.png" />
    <Overlay>
      <Row justify="space-between">
        <Col>
          <InstagramOutlined style={iconStyle} />
          <FacebookOutlined style={iconStyle} />
        </Col>
        <Col>
          <Text fontSize={18}>{i18n.login}</Text>
        </Col>
      </Row>
      <Clearfix height={50} />
      <Row justify="end" gutter={10} align="middle">
        <Col>
          <ImageWrapper>
            <Image src="/images/mrkiosk-1st-badge.png" width={34} height={34} layout="fixed" />
          </ImageWrapper>
        </Col>
        <Col>
          <Text fontSize={14}>{i18n.service}</Text>
        </Col>
      </Row>
      <Clearfix height={15} />
      <Row justify="end" gutter={10} align="middle">
        <Col>
          <ImageLogoWrapper>
            <img src="/images/mrkiosk-logo-main-title.png" />
          </ImageLogoWrapper>
        </Col>
      </Row>
      <Clearfix height={30} />
      <Row justify="end" gutter={10} align="middle">
        <Col>
          <Text fontSize={20}>{i18n.question}</Text>
        </Col>
      </Row>
      <Clearfix height={15} />
      <Row justify="end" gutter={10} align="middle">
        <Col md={9} xs={24}>
          <SearchInput placeholder={i18n.inputAddress} />
        </Col>
      </Row>
      <Clearfix height={15} />
      <Row justify="end" gutter={10} align="middle">
        <Col md={10} xs={24}>
          <Text fontSize={16} align='center' display="inline-flex">{i18n.getall}</Text>
        </Col>
      </Row>
      <ServiceWrapper>
        <Row justify="space-around" align="middle">
          <Col span={24}>
            <Text fontSize={25} fontWeight={600} color="#931832" align="center">{i18n.howto}</Text>
          </Col>
          {[
            {
              text: i18n.steps.input,
              img: '/images/mrkiosk-box1-enter-address.png',
            },
            {
              text: i18n.steps.choose,
              img: '/images/mrkiosk-box2-order.png',
            },
            {
              text: i18n.steps.dilivered,
              img: '/images/mrkiosk-box3-delivery.png',
            },
          ].map(item => (
            <Service xs={14} md={8} key={item.text}>
              <Row align="middle" gutter={[20, 25]}>
                <Col>
                  <ImageWrapper>
                    <img src={item.img} alt="" />
                  </ImageWrapper>
                </Col>
                <Col>
                  <Text color="#000000">{item.text}</Text>
                </Col>
              </Row>
            </Service>
          ))}
        </Row>
      </ServiceWrapper>
    </Overlay>
  </HeaderLayout>
)

export {Header}
