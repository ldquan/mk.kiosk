import styled from 'styled-components';

const Clearfix = styled.div`
  ${(props) => (props.height ? `height: ${props.height}px` : '')}
  ${(props) => (props.width ? `width: ${props.width}px` : '')}
`;

export {Clearfix};
