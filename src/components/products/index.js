import {Text, Clearfix} from '@/components'
import {Row, Col} from 'antd'
import styled from 'styled-components'

const Layout = styled.div`
  margin-top: 40px;
`

const Product = styled(Col)`
  &>div{
    flex-direction: column;
  }
`
const Banner = styled.div`
  position: relative;
  & > img {
    margin: 0 auto; 
    @media only screen and (max-width: 512px) {
      display: none
    }
  }
`
const Overlay = styled(Row)`
  position: absolute;
  right: 0;
  top: 0;
  padding: 60px 100px;
  @media only screen and (max-width: 512px) {
    position: relative;
    background: #fdc854;
    width: 100%;
    padding: 20px;
    img {
      width: 95%
    }
    .orderNow{
      font-size: 30px
    }
  }
`

const i18n = {
  productlist: 'Mr. Kiosk delivers products such as...',
  products: {
    Milk: 'Milk, Yoghurt & Dairy',
    Biscuits: 'Biscuits & Sweets',
    Cleaning: 'Cleaning & Hygeine',
    Nuts: 'Nuts, Dry Fruit & Cereals',
    Alcohol: 'Alcohol & Tobacco',
  },
  orderFromYourPhone: 'Order from your phone!',
  accept: 'We accept credit card and cash payments!'
}

const Products = () => {
  return (
    <Layout>
      <Text fontSize={22} align="center" color="#931832" >{i18n.productlist}</Text>
      <Clearfix height={25} />
      <Row gutter={[0, 40]} justify="center">
        {[
          {
            text: i18n.products.Milk,
            img: '/images/mrkiosk-cat-milk-dairy.jpg',
          },
          {
            text: i18n.products.Biscuits,
            img: '/images/mrkiosk-cat-biscuits.jpg',
          },
          {
            text: i18n.products.Cleaning,
            img: '/images/mrkiosk-cat-hygeine.jpg',
          },
          {
            text: i18n.products.Nuts,
            img: '/images/mrkiosk-cat-nuts-fruit-cereal.jpg',
          },
          {
            text: i18n.products.Alcohol,
            img: '/images/mrkiosk-cat-alcohol-tobacco.jpg',
          },
        ].map(item => (
          <Product xs={24} md={4} key={item.text}>
            <Row align="middle" gutter={[0, 10]}>
              <Col>
                <img src={item.img} alt="" />
              </Col>
              <Col>
                <Text color="#000000">{item.text}</Text>
              </Col>
            </Row>
          </Product>
        ))}
      </Row>
      <Clearfix height={30} />
      <Row justify="center">
        <Col>
          <Banner>
            <img src="/images/mrkiosk-app-footer-bg.jpg" alt="" />
            <Overlay gutter={[0, 110]} justify="end">
              <Col>
                <img src="/images/mrkiosk-app-logo.png" />
                <Text fontSize={40} className="orderNow" align="right">{i18n.orderFromYourPhone}</Text>
                <Text fontSize={18} color="#000000" align="right">{i18n.accept}</Text>
              </Col>
              <Col>
                <Row gutter={20} justify="center">
                  <Col md={12} xs={10}>
                    <img src="/images/mrkiosk-app-store.png" />
                  </Col>
                  <Col md={12} xs={10}>
                    <img src="/images/mrkiosk-play-store.png" />
                  </Col>
                </Row>
              </Col>
            </Overlay>
          </Banner>
        </Col>
      </Row>
    </Layout>
  )
}

export {Products}
