import {Col, Row} from 'antd'
import styled from 'styled-components'
import {Text} from '@/components'
import {Clearfix} from '../clearfix'

const Layout = styled.div`
  margin-top: 5vh;
  background: #181818;

  padding: 10vh 10vw;
  @media only screen and (max-width: 512px) {
    padding: 4vh 4vw;
  }
`

const i18n = {
  name: 'Mr. Kiosk - Your Kiosk at Your Door.',
  foody: 'As seen online on foody.com.cy',
  register: 'The Mr. Kiosk Happy Man Logo and Name are registered trademarks owned by',
  year: 'NO. Mr Kiosk Ltd - 2020 2021. All Rights reserved',
  aboutUsText: 'About Us',
  aboutUs: 'F.A.Q.S',
  contact: 'Contact Us',
  locations: 'Locations',
  Nicosia: 'Nicosia',
  Limassol: 'Limassol',
  Larnaka: 'Larnaka'
}

const Footer = () => {
  return (
    <Layout>
      <Row justify="center" gutter={[20, 20]}>
        <Col md={8} xs={24}>
          <img src="/images/mrkiosk-footer.png" />
        </Col>
        <Col md={8} xs={24}>
          <Text>{i18n.name}</Text>
          <Text>{i18n.foody}</Text>
        </Col>
        <Col md={8} xs={24}>
          <Row justify="space-around">
            <Col span={8}>
              <Text>{i18n.aboutUsText}</Text>
              <Text>{i18n.aboutUs}</Text>
              <Text>{i18n.contact}</Text>
            </Col>
            <Col span={8}>
              <Text>{i18n.locations}:</Text>
              <Text fontSize={13}>{i18n.Nicosia}</Text>
              <Text fontSize={13}>{i18n.Limassol}</Text>
              <Text fontSize={13}>{i18n.Larnaka}</Text>
            </Col>
          </Row>
        </Col>
      </Row>
      <Clearfix height={10}/>
      <Row>
        <Col span={24}>
          <Text fontSize={13}>{i18n.register}</Text>
          <Text fontSize={13}>{i18n.year}</Text>
        </Col>
      </Row>
    </Layout>
  )
}

export {Footer}
