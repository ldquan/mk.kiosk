import {Header, Products, Footer} from '@/components'
import styled from 'styled-components'

const Layout = styled.div` `

export default function Home() {
  return (
    <Layout>
      <Header />
      <Products />
      <Footer />
    </Layout>
  )
}

