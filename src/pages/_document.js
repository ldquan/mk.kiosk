import Document, {Head, Main, NextScript} from 'next/document';
import {ServerStyleSheet} from 'styled-components';

export default class MyDocument extends Document {
  static getInitialProps({renderPage}) {
    // Step 1: Create an instance of ServerStyleSheet
    const sheet = new ServerStyleSheet();

    // Step 2: Retrieve styles from components in the page
    const page = renderPage((App) => (props) =>
      sheet.collectStyles(<App {...props} />),
    );

    // Step 3: Extract the styles as <style> tags
    const styleTags = sheet.getStyleElement();

    const googleMapUrl = `https://maps.googleapis.com/maps/api/js?key=${process.env.GOOGLE_MAP_KEY}&libraries=places,geometry`

    // Step 4: Pass styleTags as a prop
    return {...page, styleTags, googleMapUrl};
  }

  render() {
    const {styleTags, googleMapUrl} = this.props
    return (
      <html>
        <Head>
          <title>MR.KIOSK</title>
          {/* Step 5: Output the styles in the head  */}
          {styleTags}
          <script defer src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
          <script defer src={googleMapUrl} />
        </Head>
        <body>
          <div id="map" />
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
