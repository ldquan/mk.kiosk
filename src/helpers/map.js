import {PolygonPaths} from "@/constants";

const getDistances = async ({origins, destinations, travelMode = 'DRIVING'}) => {
  const service = new google.maps.DistanceMatrixService();

  return new Promise((res) => {
    const params = {
      origins,
      destinations,
      travelMode
    }
    const callback = (response, status) => {
      if (status !== 'OK') {
        console.log(status)
        return;
      }
      res({status, data: response})
    }
    service.getDistanceMatrix(params, callback);
  })
}

const getPlaces = async (query, country) => {
  const map = document.getElementById('map')
  const service = new google.maps.places.PlacesService(map);
  return new Promise((res) => {
    const params = {query}
    const callback = (response, status) => {
      if (status !== 'OK') {
        console.log(status)
        return;
      }

      const data = response.filter(item => !country || item.formatted_address.match(new RegExp(country, 'i')));
      res({status, data})
    }
    service.textSearch(params, callback)
  })
}

const setupMap = (options) => {
  const initOptions = options
    ? options
    : {
      zoom: 12,
      center: {lat: 34.909384084555235, lng: 33.63311479473013},
    }

  const map = new google.maps.Map(document.getElementById('map'), initOptions);

  const configs = Object.keys(PolygonPaths).map(key => ({
    paths: PolygonPaths[key]
  }))

  const polygons = configs.map(item => new google.maps.Polygon({
    paths: item.paths,
    //editable: true,
    strokeWeight: 3,
  }))

  polygons.forEach(item => {
    google.maps.event.addListener(item, 'click', function () {
      item.getPath().forEach(ele => {
        console.log(`{lat: ${ele.lat()}, lng: ${ele.lng()}}`)
      })
    })
    item.setMap(map);
  });
}

export function checkContainsLocation(latLng, paths) {
  const polygon = new google.maps.Polygon({paths})
  const result = google.maps.geometry.poly.containsLocation(
    latLng,
    polygon
  )
  return result
}

export {setupMap, getDistances, getPlaces}
